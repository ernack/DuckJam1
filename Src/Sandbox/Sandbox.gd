extends Node2D

var spawn_turn : int = 0
var shaking : bool = false

func _ready():
	
	GMap.init_map($TileMap)
	GTurn.connect("on_next_turn", self, '_on_next_turn')
	GEvents.sandbox = self
	randomize()
	var tmp = null
	var i = 0
	for characters in $Team.get_children():
		i += 1
		characters.init_player()
		tmp = GMap.free_position()
		characters.set_grid_position(tmp.x, tmp.y)
		characters.connect('on_dying', self, '_on_dying')
	GEvents.connect("on_shake", self, '_on_shake')
	
func _process(delta):
	if GCharacters.get_players().empty():
		GCharacters.characters.clear()
		for c in GEvents.get_children():
			GEvents.remove_child(c)
		get_tree().change_scene('res://EndScreen/EndScreen.tscn')
	
	if GSelected.selected != null:
		$Camera2D.position = (GSelected.selected.position)
	
	if Input.is_action_just_pressed("select_next"):
		GCharacters.select_next()
	
	for b in $Blood.get_children():
		var bb : Sprite = b
		if bb.scale.length_squared() < 2.0*2.0:
			bb.scale += Vector2(delta, delta)
			
	if self.shaking:
		var offset : float = 32
		$Camera2D.position += Vector2(rand_range(-offset, offset), rand_range(-offset, offset))

func _on_next_turn():
	pass
		
func _on_SpawnTimer_timeout():
	self.shaking = false

func put_blood(pos):
	var sprite : Sprite = Sprite.new()
	sprite.texture = preload("res://Assets/blood.png")
	sprite.position = pos
	sprite.region_enabled = true
	sprite.z_index = -1
	randomize()
	var rand_x = round(rand_range(0, 4))
	var rand_y = round(rand_range(0, 4))
	sprite.region_rect = Rect2(Vector2(GMap.TILE_WIDTH*rand_x, GMap.TILE_HEIGHT*rand_y), Vector2(GMap.TILE_WIDTH, GMap.TILE_HEIGHT))
	$Blood.add_child(sprite)
	GAudio.splash()

func _on_dying(character):
	put_blood(character.position)
	
func _on_shake():
	$ShakeTimer.wait_time = 2.0
	$ShakeTimer.start()
	self.shaking = true
