extends Control

func _ready():
	pass

func _process(delta):

	if GSelected.selected != null:
		$Panel/ActionLabel.visible = true
		$Panel/AmmoLabel.visible = true
		
		$Panel/ActionLabel.text = 'AP: ' + str(GSelected.selected.action_points)
		$Panel/AmmoLabel.text = 'TEAM: ' + str(GCharacters.get_players().size())
	else:
		$Panel/ActionLabel.visible = false
		$Panel/AmmoLabel.visible = false
	
	if no_more_action():
		$Panel/TurnLabel.modulate = Color.yellow
	else:
		$Panel/TurnLabel.modulate = Color.white
		
	$Panel/TurnLabel.text = 'Turn: ' + str(GTurn.current)

func no_more_action():
	for c in GCharacters.get_players():
		if c.action_points != 0:
			return false
	return true
