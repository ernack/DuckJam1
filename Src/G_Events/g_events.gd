extends Node
var storm_timer : Timer
var storm_show_timer : Timer
var storm_count = 0
var storm_sprite : Sprite = Sprite.new()

var sandbox = null

signal on_storm
signal on_spawn
signal on_shake

func _ready():
	GTurn.connect("on_next_turn", self, '_on_next_turn')
	self.storm_show_timer = Timer.new()
	self.storm_show_timer.stop()
	self.storm_show_timer.wait_time = 0.1
	self.storm_show_timer.connect("timeout", self, '_on_storm_show_timer')
	
	self.storm_timer = Timer.new()
	self.storm_timer.autostart = false
	self.storm_timer.wait_time = 1.0
	self.storm_timer.connect("timeout", self, '_on_storm_timer')
	
	add_child(self.storm_show_timer)
	add_child(self.storm_timer)
	
	self.storm_sprite.texture = preload("res://Assets/storm.png")
	
func _on_next_turn():
	var choice = round(rand_range(0, 4))
	
	if choice == 0:
		emit_signal("on_shake")
		GAudio.shaking()
		for c in GCharacters.characters:
			if c and c.has_method('hurt'):
				c.hurt(rand_range(5, 20))
	elif choice == 1:
		start_storm_event()
	else:
		monster_event()
		
	
func _on_storm_timer():
	self.storm_timer.wait_time = rand_range(0.05, 0.2)
	if self.storm_count > 0:
		self.storm_count -= 1
		storm_event()
		emit_signal("on_storm")
	
func _on_storm_show_timer():
	remove_child(self.storm_sprite)

func start_storm_event():
	self.storm_count = round(rand_range(8, 32))
	storm_event()
	
func storm_event():
	self.storm_timer.start()
	var i = round(rand_range(0, GMap.HEIGHT - 1))
	var j = round(rand_range(0, GMap.WIDTH - 1))
	
	self.storm_sprite.position.x = j * GMap.TILE_WIDTH
	self.storm_sprite.position.y = i * GMap.TILE_HEIGHT - self.storm_sprite.get_rect().size.y/2 + GMap.TILE_HEIGHT
	
	add_child(self.storm_sprite)
	self.storm_show_timer.start()
	var character = GCharacters.is_position_taken(i, j)
	if character:
		character.hurt(95.0)
		
func monster_event():
	emit_signal("on_spawn", self)
	var monster = preload("res://Character/Character.tscn").instance()
	monster.connect("on_dying", self.sandbox, '_on_dying')
	monster.init_monster()
	monster.connect('on_dying', self, '_on_dying')
	var tmp = GMap.free_position()
	monster.set_grid_position(tmp.x, tmp.y)
	add_child(monster)
