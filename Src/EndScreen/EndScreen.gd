extends Node2D


func _ready():
	GTurn.current = 0
	if GCharacters.get_players().empty():
		$Control/VBoxContainer/Label.text = "YOU FAILED"
	else:
		$Control/VBoxContainer/Label.text = "YOU WON"
	GCharacters.characters.clear()



func _on_Button_button_down():
	get_tree().change_scene("res://TitleScreen/TitleScreen.tscn")
