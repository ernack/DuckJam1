extends Node

const WIDTH : int = 16
const HEIGHT : int = 16
const TILE_WIDTH : int = 64
const TILE_HEIGHT : int = 64
var map : Array = []
var tilemap = null

func _ready():
	pass

func init_map(tilemap : TileMap):
	self.tilemap = tilemap
	self.tilemap.z_index = -256
	self.tilemap.cell_size = Vector2(GMap.TILE_WIDTH, GMap.TILE_HEIGHT)
	for i in range(0, GMap.HEIGHT):
		for j in range(0, GMap.WIDTH):
			if i > 0 and j > 0 and i < GMap.HEIGHT - 1 and j < GMap.WIDTH - 1:
				self.tilemap.set_cell(j, i, 0)
				map.push_back(0)
			else:
				self.tilemap.set_cell(j, i, 1)
				map.push_back(1)
				
func is_obstacle(i : int, j : int) -> bool:
	if i * GMap.WIDTH + j >= self.map.size():
		return true
	
	return self.map[i * GMap.WIDTH + j] != 0
	
func snap_position(pos : Vector2) -> Vector2:
	return Vector2(int(pos.x/TILE_WIDTH) * TILE_WIDTH, int(pos.y/TILE_HEIGHT) * TILE_HEIGHT)

func free_position():
	var res = Vector2()
	res.x = 1 + randi() % (GMap.WIDTH - 2)
	res.y = 1 + randi() % (GMap.HEIGHT - 2)
	if GCharacters.is_position_taken(res.y, res.x):
		return free_position()
	return res
