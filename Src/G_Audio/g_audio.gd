extends Node

var audio : Array = []

var storm : AudioStream
var turn : AudioStream
var spawn : AudioStream
var select : AudioStream
var punch : AudioStream
var splash : AudioStream
var shaking : AudioStream

func _ready():
	for i in range(0, 16):
		self.audio.push_back(AudioStreamPlayer.new())
		add_child(self.audio[i])
		
	self.storm = preload("res://Assets/storm.wav")
	GEvents.connect("on_storm", self, '_on_storm')
	
	self.turn = preload("res://Assets/turn.wav")
	GTurn.connect("on_next_turn", self, '_on_turn')
	
	self.spawn = preload("res://Assets/spawn.wav")
	GEvents.connect("on_spawn", self, '_on_spawn')
	
	self.select = preload("res://Assets/select.wav")
	GSelected.connect("on_select", self, '_on_select')
	
	self.punch = preload("res://Assets/punch.wav")
	
	self.splash = preload("res://Assets/splash.wav")
	
	self.shaking = preload("res://Assets/shaking.wav")
	
func _on_storm():
	self.audio[5].stream = self.storm
	self.audio[5].pitch_scale = rand_range(0.5, 1.5)
	self.audio[5].play()

func _on_turn():
	self.audio[0].stream = self.turn
	self.audio[0].play()
	
func _on_spawn(monster):
	self.audio[1].stream = self.spawn
	self.audio[1].play()
	self.audio[1].pitch_scale = rand_range(1.0, 3.0)

func _on_select():
	self.audio[2].stream = self.select
	self.audio[2].play()

func punch():
	self.audio[3].stream = self.punch
	self.audio[3].play()
	
func splash():
	self.audio[4].stream = self.splash
	self.audio[4].play()

func shaking():
	self.audio[5].stream = self.shaking
	self.audio[5].play()
