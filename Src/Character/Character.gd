extends Node2D

var mouse_in : bool = false
var action_points : int = GActions.CHARACTER_ACTION_POINTS
var grid_pos : Vector2 = Vector2(0, 0)
var dead : bool = false
var dying : bool = false

var old_dead = false

var life : float = 100
var atk : float = 8.0
var lethal_atk : float = 200.0
var def : float = 1.0

var controller;
var player : bool = false

signal on_dying
signal on_dead

func init_player():
	self.controller = $Controller/Player.init(self)
	self.player = true
		
func init_monster():
	self.controller = $Controller/Monster.init(self)

func _ready():
	GCharacters.add(self)
	GTurn.connect("on_next_turn", self, '_on_next_turn')

func _on_next_turn():
	self.action_points = GActions.CHARACTER_ACTION_POINTS
	if self.dying:
		hurt(10)
		
func _process(delta):
	if self.dead: 
		return
	
	# Select character.
	if Input.is_action_just_pressed("SelectCharacter") and self.mouse_in:
		GSelected.selected = self
	
	# Character movements.
	if self.controller:
		self.controller.process()
	
	# Update color.
	if GSelected.selected == self:
		$Sprite.modulate = Color(1.0, 1.0, 1.0, 1.0)
	else:
		$Sprite.modulate = Color(0.7, 0.7, 0.7, 1.0)
	
	# Update position.
	var i = self.grid_pos.y * GMap.TILE_HEIGHT + GMap.TILE_HEIGHT/4
	var j = self.grid_pos.x * GMap.TILE_WIDTH + GMap.TILE_WIDTH/2
	position = Vector2(j, i)
	
	# Update life.
	$LifePercent.value = self.life
	if self.life <= 0:
		self.dead = true
		queue_free()
		self.modulate = Color.black
		return
		
	# Update action.
	$ActionPercent.value = 100.0 * (float(self.action_points)/GActions.CHARACTER_ACTION_POINTS)
	
func hurt(damage):
	var final_damage = damage
	GAudio.punch()
	var up = GCharacters.is_position_taken(self.grid_pos.y-1, self.grid_pos.x)
	var down = GCharacters.is_position_taken(self.grid_pos.y+1, self.grid_pos.x)
	var left = GCharacters.is_position_taken(self.grid_pos.y, self.grid_pos.x-1)
	var right = GCharacters.is_position_taken(self.grid_pos.y, self.grid_pos.x+1)
	
	if (up or self.grid_pos.y == 1) and \
		(down or self.grid_pos.y == GMap.HEIGHT-2) and \
		(left or self.grid_pos.x == 1) and \
		(right or self.grid_pos.x == GMap.WIDTH-2):
		final_damage = self.lethal_atk
		
	self.life -= (final_damage - self.def)
	self.dying = self.life < 50.0
	
	
func process_character_move(key : String, pos : Vector2):
	if GMap.is_obstacle(self.grid_pos.y + pos.y, self.grid_pos.x + pos.x):
		return
		
	if GSelected.selected == self and \
		self.action_points - GActions.CHARACTER_MOVE_COST >= 0 \
		and Input.is_action_just_pressed(key):
			var other = GCharacters.is_position_taken(self.grid_pos.y + pos.y, self.grid_pos.x + pos.x)
			if other != null and not other.dead:
				if self.action_points >= GActions.CHARACTER_PUNCH_COST:
					self.action_points -= GActions.CHARACTER_MOVE_COST
					other.hurt(self.atk)
					return
				return
			self.action_points -= GActions.CHARACTER_MOVE_COST
			self.grid_pos += pos
			if self.dying:
				emit_signal("on_dying", self)
			
		
	
func set_grid_position(i : int, j : int):
	self.grid_pos = Vector2(i, j)
	
func _on_Area2D_mouse_entered():
	self.mouse_in = true

func _on_Area2D_mouse_exited():
	self.mouse_in = false
