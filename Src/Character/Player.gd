extends Node

var character = null

func init(character):
	self.character = character
	
func _ready():
	pass

func _process(delta):
	if self.character:
		self.character.process_character_move("character_left", Vector2(-1, 0))
		self.character.process_character_move("character_right", Vector2(1, 0))
		self.character.process_character_move("character_up", Vector2(0, -1))
		self.character.process_character_move("character_down", Vector2(0, 1))
