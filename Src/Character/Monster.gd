extends Node

var character = null
var target = null

func init(character):
	self.character = character
	GTurn.connect("on_next_turn", self, '_on_next_turn')
	self.character.life = rand_range(50, 200)
	
	var sprite = self.character.get_node('Sprite')
	
	var choice = round(rand_range(0, 3))
	print(choice)
	if choice == 0:
		sprite.texture = preload("res://Assets/monster.png")
	elif choice == 1:
		sprite.texture = preload("res://Assets/monster2.png")
	elif choice == 2:
		sprite.texture = preload("res://Assets/monster3.png")
	elif choice == 3:
		sprite.texture = preload("res://Assets/monster4.png")
		
	var scale = rand_range(0.25, 0.75)
	sprite.scale = Vector2(scale, scale)
	sprite.modulate = Color.black
	
func _process(delta):
	if self.target == null or self.target.dead:
		self.target = GCharacters.random_player()

func _ready():
	pass

func _on_next_turn():
	if self.character.dying:
				self.character.emit_signal("on_dying", self.character)
				
	var dir = Vector2(0, 0)
	if self.target == null:
		return
	
	if self.character.position.x < self.target.position.x:
		dir.x = 1
	elif self.character.position.x > self.target.position.x:
		dir.x = -1
	
	if self.character.position.y < self.target.position.y:
		dir.y = 1
	elif self.character.position.y > self.target.position.y:
		dir.y = -1
		
	if dir.x + dir.y > 1:
		if randf() > 0.5:
			dir.x = 0
		else:
			dir.y = 0
	
	if GMap.is_obstacle(self.character.grid_pos.y + dir.y, self.character.grid_pos.x + dir.x):
		return
		
	var other = GCharacters.is_position_taken(self.character.grid_pos.y + dir.y, self.character.grid_pos.x + dir.x)
	if other != null and not other.dead:
		if self.character.action_points >= GActions.CHARACTER_PUNCH_COST:
			other.hurt(self.character.atk)
			return
		return
	
	self.character.grid_pos += dir
	self.character.action_points -= GActions.CHARACTER_MOVE_COST
	
