extends Node

var selected = null
var old_selected = null
signal on_select

func _ready():
	pass

func _process(delta):
	if self.selected != self.old_selected:
		self.old_selected = self.selected
		emit_signal("on_select")
