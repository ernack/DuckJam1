extends Node

var characters : Array = []
var current : int = 0
var player_count : int = 0

func _ready():
	pass
	
func add(c):
	self.characters.push_back(c)
	if c.player:
		self.player_count += 1

func is_position_taken(i : int, j : int):
	for c in self.characters:
		if c and c.grid_pos.x == j and c.grid_pos.y == i:
			return c
	return null

class PlayerSorter:
	static func sort(a, b):
		if b.action_points > a.action_points:
			return true
		return false
		
func select_next():
	var players = get_players()
	if players.empty() or GSelected.selected == null or self.current >= players.size():
		return
	players.sort_custom(PlayerSorter, 'sort')
	GSelected.selected = players[self.current]
	self.current += 1
	if self.current >= players.size():
		self.current = 0
	
func get_players():
	var players = []
	for c in self.characters:
		if c and c.player:
			players.push_back(c)
	return players
	
func random_player():
	var players = get_players()
	
	if players.empty():
		return null
	return players[randi() % players.size()]

