extends Node

var current : int = 0
signal on_next_turn

func _ready():
	pass

func next_turn():
	current += 1
	emit_signal("on_next_turn")
	
func _process(delta):
	if GCharacters.get_players().empty():
		return
		
	if Input.is_action_just_pressed("force_next_turn"):
		self.next_turn()
		return
	elif Input.is_action_just_pressed("next_turn"):
		var players = GCharacters.get_players()
		for p in players:
			if p.action_points > 0:
				GSelected.selected = p
				return
		
		self.next_turn()
